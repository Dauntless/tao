"""
@author: stoner
"""

from tao.service.managed_service import ManagedClass
from tao.interfaces.interfaces import FooIFace, Configurator


class Foo(FooIFace, ManagedClass):
    def __init__(self, x):
        self.x = x

    def doSomething(self, x):
        return 2 * x
    
    
class CommandLineConfig(Configurator):
    pass


class YAMLConfig(Configurator):
    pass


class INIConfig(Configurator):
    pass


class XMLConfig(Configurator):
    pass
