
from tao.manager.manager import ServiceManager
from tao.manager.manager_exceptions import NotRegisteredError
from tao.core.logger import glob_logger


class ManagedClass(object):
    """
    Metaclass used to create subclasses that must be managed by a ServiceManager
    
    The idea behind this class is to use it as a template to create classes
    that must be managed by a ServiceManager.  If a class _is a_ ManagedClass,
    then it can not be instantiated directly.  ManagedClass subclasses should
    inherit from ManagedInterface as well.  To create an instance of this 
    object, first the ManagedInterface type should be registered, then the 
    class of the object itself.  If these are not registered, then instantiation
    will fail.  This prevents users from "accidentally" circumventing creating
    a concrete object instead of an object which fulfills the contract.
    """
    def __new__(cls, *args, **kwds):
        """
        Check to see if a class has an interface registered and a manger
        """
        glob_logger("cls = {}".format(cls))
        glob_logger("pos args:")
        for x in args:
            glob_logger("\t{}".format(x))
        glob_logger("keyword args:")
        for k,v in kwds.items():
            glob_logger("{}={}".format(k,v))

        ## If subclass does not define manager, do it for them
        if "manager" in kwds and isinstance(kwds["manager"], ServiceManager):
            glob_logger("manager already in class")
        elif "manager" not in kwds or \
            not isinstance(kwds["manager"], ServiceManager):
            kwds["manager"] = ServiceManager()

        registered = cls.checkIfRegisteredService()
        if not registered:
            glob_logger("This class is not registered with the Service Manager")
            raise NotRegisteredError()

        return super(ManagedClass, cls).__new__(cls, *args, **kwds)     

    @classmethod
    def checkIfRegisteredIFace(cls, mgr=ServiceManager()):
        """
        Determines if the class (service) is registered with the manager
        
        Args:
        cls(type)-  A class which inherits from ManagedInterface
        """
        bases = cls.__bases__
        for base in bases:
            if base in mgr.contracts:
                return True
        return False
    
    @classmethod
    def checkIfRegisteredService(cls, mgr=ServiceManager()):
        """
        Determines if the class (service) is registered with the manager
        
        Args:
        cls(type)-  A class which inherits from ManagedInterface
        """
        if cls in mgr.services:
            return True
        return False

    def __init__(self, name, bases, dct):
        glob_logger("==================\nIn __init__")
        glob_logger("self = {}".format(self))
        glob_logger("bases = {}".format(bases))
        glob_logger("dct = {}".format(dct))
        if not hasattr(self, 'registry'):
            # this is the base class.  Create an empty registry
            self.registry = {}
        else:
            # this is a derived class.  Add cls to the registry
            interface_id = name.lower()
            self.registry[interface_id] = self
            
        super(ManagedClass, self).__init__(name, bases, dct)


