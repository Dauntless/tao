from subprocess import Popen, PIPE, STDOUT
import threading
import os
import time
import shlex

from tao.core import logger
# from tao.interfaces.itask import ITask
# from tao.service.managed_service import ManagedClass

LOG_DIR = "logs"
if not os.path.exists("logs"):
    os.mkdir("logs")
LOGGER = logger.glob_logger
os.environ["PYTHONUNBUFFERED"] = "1"


def freader(fobj, interval=0.2):
    """
    Small function which can be thrown into a thread to read a long running
    subprocess

    :param fobj:
    :param interval:
    :return:
    """
    buff = ""  # FIXME: StringIO?  The problem is that python2 will require
               # unicode strings if using io.StringIO
    while not fobj.closed:
        buff += fobj.readline()
        time.sleep(interval)
    return buff


class Result:
    def __init__(self, rc, msg="", data=None):
        self.result = rc
        self.description = msg
        if data is None:
            self.data = {}
        else:
            self.data = data


class ProcessResult:
    """
    Represents the result of a subprocess.

    Because we might run the subprocess in a non-blocking manner (ie, not
    calling communicate), this class represents the current state of the
    process.  That means we may not have the returncode, stdout or stderr
    yet.

    This class models a "truthiness" value so a ProcessResult object can
    be used in truth value testing.  It also implements the == operator
    to make it easier to do return code checking.  This was done because
    subclassing from int made no sense if the subprocess was not complete
    since the result of popen_obj.poll() or popen_obj.returncode would be
    None, and int types must have an int value ('inf' and 'nan' are for
    float types)
    """
    def __init__(self, process, outp="", error="", logger=LOGGER):
        self.logger = logger
        self.proc = process
        self._output = outp
        self._error = error
        self.returncode = process.poll()

    def __nonzero__(self):
        """
        Allows the ProcessResult object to be used for truthiness

        example::

            result = ProcessResult(proc)
            if result:
                print result.output
        """
        if self.returncode is not None:
            return True
        return False

    def __eq__(self, other):
        """
        Allows the ProcessResult object to be used for int equality
        checking.

        example::

            result = ProcessResult(proc)
            if result == 0:
                print("subprocess was successful")
        """
        if self.returncode == other:
            return True
        else:
            return False

    def _check_filehandle(self, fh_name="stdout"):
        try:
            stdout = getattr(self.proc, fh_name)
            if not stdout.closed:
                return stdout
        except AttributeError:
            # TODO: no self.proc.stdout, check if self.proc is a file handle
            raise
        return None

    def _get_from_file(self, fobj, blocking=True):
        if not blocking:
            rdr_thread = threading.Thread(target=freader, args=(fobj,))
            rdr_thread.start()


    @property
    def output(self):
        if self.returncode is None:
            self.logger.warning("Process is not yet finished")
        if not self._output:
            out = self._check_filehandle()
            self._output = out.read()
        return self._output

    @output.setter
    def output(self, val):
        self.logger.error("output is read-only. Not setting to {}".format(val))


class CommandException(Exception):
    def __init__(self,msg=""):
        super(CommandException,self).__init__()
        self.msg = msg


class Command(object):
    """
    A class to handle executing subprocesses.  The intention is to allow a
    simpler way to handle threading or multiprocessing functionality.  This 
    also allows the caller to chose to block (waiting for the subprocess to 
    return) or not.
    """
    def __init__(self, cmd=None, sudo=False, pw="", logr=None, stdin=PIPE,
                 stdout=PIPE, stderr=STDOUT):
        """
        *args*
            cmd(str|list)- The command to be executed, either in string or list format
            logr(Logger)- a logging.Logger instance. if None, use the module LOGGER
            stdout(file-like)- by default uses PIPE, but can be any file-like object
            stderr(file-like)- same as stdout
        """
        self.cmd = cmd
        self.out = stdout
        self.err = stderr
        self.inp = stdin
        self.fails = {}
        self.sudo = sudo
        self.pw = pw
        if logr:
            self.logger = logr
        else:
            self.logger = LOGGER

    def __call__(self,
                 cmd=None,
                 showout=True, 
                 showerr=True, 
                 block=True, 
                 checkresult=(True,0),
                 **kwds):
        """
        This is a wrapper around subprocess.Popen constructor.  The **kwds 
        takes the same keyword args as the Popen constructor does.  During
        initialization, by default PIPE will be used for the stdout and stderr,
        however, file-like objects may be passed in instead (for example to 
        log)
        
        *args*
            cmd(str|list)- the command and arguments to run
            showout(bool)- whether to show output or not
            showerr(bool)- whether to show stderr or not
            block(bool)- if false, return immediately, else wait for subprocess
            checkresult((bool,int))- tuple; first element is to do checking, 
            second is success return code

        *return*
            A tuple of (subprocess.process, string, string) representing the 
            subprocesses process object, and string from the stdout and stderr 
            of the child process respectively (all might be None).
        """
        if not cmd and not self.cmd:
            raise CommandException("Must have a command to execute")
        if cmd:
            self.cmd = cmd

        if isinstance(self.cmd, str):
            cmd_toks = shlex.split(self.cmd)
        else:
            cmd_toks = self.cmd

        if self.sudo:
            cmd_toks = ["sudo"] + cmd_toks

        kwds['stdout'] = self.out
        kwds['stderr'] = self.err

        ## Setup our return vals
        output = None
        err = None

        if block:
            proc = Popen(cmd_toks, **kwds)
            (output, err) = proc.communicate()  # FIXME: what about input?
            if showout and output: 
                self.logger.info(output)
            if showerr and err: 
                self.logger.error(err)
        else:
            proc = Popen(cmd_toks, **kwds)

        self.proc = proc
        result = (proc, output, err)
        if checkresult[0]:
            self.checkResult(result, checkresult[1])
            
        return ProcessResult(*result)

    def checkResult(self, result, success=0):
        """
        Simple checker for the return of a subprocess.

        *args*
            result(tuple)- same type as return from __call__()
            success(int)- the return code that indicates success
        """
        proc, output, err = result
        returnval = proc.poll()
        if returnval == None:
            self.logger.warn("Process is still running")
            return None
        elif proc.returncode != success:
            self.logger.error("ReturnCode: {0}".format(returnval))
            self.logger.error("stderr: {0}".format(err))
            self._addFail(result)
        else:
            self.logger.info("Process was successful")
        return returnval

    def _addFail(self, result):
        proc,out,err = result
        self.fails[proc.pid] = {"returncode" : proc.returncode,
                                "errmsg" : err,
                                "command" : self.cmd }

    def make_proxy(self, handler):
        """
        Spawns a thread so that we can read the stdout of a long running subprocess

        *args*
            res(tuple)- This is the same as the return from __call__()

        *return*
            The thread object which will monitor the stdout of the subprocess

        Usage::
        
            cmd = Command()
            result = cmd("python some_long_script.py --time=240", block=False)
            printer_thread = cmd.printRunningProc(result)
            printer_thread.start()
            printer_thread.join()  ## only if your parent thread might finish before the thread 

        PostCondition-
            When using this function, make sure that after calling start() on the 
        thread, that the child thread may not outlive the parent thread.  This will
        result in undefined behavior.            
        """

        class CommandProxy(threading.Thread):
            def __init__(self, targ, *args, **kwds):
                super(CommandProxy, self).__init__()
                self.handler = targ
                self.args = args
                self.kwds = kwds
                
            def run(self):
                self.handler(*self.args, **self.kwds)

        return CommandProxy(handler)