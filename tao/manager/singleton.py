'''
Created on Sep 9, 2013

@author: stoner
'''

class Singleton(type):
    def __init__(self, *args, **kwds):
        self.__instance = None
        self.testing = 10
        super(Singleton, self).__init__(*args, **kwds)

    def __call__(self, *args, **kwds):
        if self.__instance == None:
            self.__instance = super(Singleton, self).__call__(*args, **kwds)
        return self.__instance
            