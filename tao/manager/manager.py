"""
Restricts
"""
from tao.manager.singleton import Singleton
from tao.core.logger import get_simple_logger


class ServiceManager(object):
    """
    The ServiceManager's job is to load, unload, and find services
    
    The ServiceManager is primarily used to enforce creation of 
    ManagedClass objects.  A ManagedClass must register with the
    ServiceManager as well as its ManagedInterface type before the
    object can actually be created
    
    Loading a service implies finding any dependencies.  In other
    words, a service may have a dependency on another service
    """
    __metaclass__ = Singleton

    def __init__(self):
        ## holds a mapping of IFace to concrete service
        ## eg. self.contracts['FooIFace'] = ['Foo']
        self.contracts = {}
        self.services  = {}   ## mapping of contract type to service type
        self.active_services = {}
        self.logger = get_simple_logger(__name__, __name__ + ".log")
        self.logger.info("Service Manager being created")
        
    def registerContract(self, contract, version=None):
        """
        Add an Interface to the manager.  
        
        This adds an extension point, or a way to add other services
        """
        if contract in self.contracts:
            return None

        ## TODO: add versioning
        
        self.contracts[contract] = []
        return self.contracts
    
    def unregisterContract(self, contract, version=None):
        if contract in self.contracts:
            for service in self.contracts[contract]:
                self.unregisterService(service)
            del self.contracts[contract]
        else:
            return ReturnCode("NotFound")
        
        
    def registerService(self, service, contract=None):
        """
        Add a service to the manager       
        Return:
            The contract if successful, None otherwise
        """
        if contract and not issubclass(service, contract):
            self.logger.info("Can not add service for this extension")
            contract = None
        else:
            for iface,services in self.contracts.items():
                self.logger.debug("iface = {0}".format(iface))
                self.logger.debug("services = {0}".format(services))
                if issubclass(service, iface):
                    contract = iface
                    self.logger.info("Matched service with iface: {0} {1}".format(service, iface))
                    if service in services:
                        self.logger.info("Service already registered")
                        contract = None
                    break
            else:
                contract = None    
        
        ## Add the service type to our mapping
        if contract:
            if self.services.has_key(service):
                self.services[service].append(contract)
            else:
                self.services[service] = [contract]

            ## and also for contracts
            if self.contracts.has_key(contract):
                self.contracts[contract].append(service)
            else:
                self.contracts[contract] = [service]
            
        return contract
    
    def unregisterService(self, service, contract=None):
        """
        """
        iface = None
        for contract in self.contracts:
            if issubclass(service, contract):
                iface = contract
                break
            
        try:
            del self.services[service]
            if service == self.active_services[iface]:
                del self.active_services[iface]
                self.active_services[iface] = None
        except:
            pass

    def setActiveService(self, service):
        """
        Specifies the service that should be created for a contract
        """
        service = None
        for srv in self.services:
            if srv == service:
                service = srv
                break
        else:
            if not self.registerService(service):
                self.logger.info("Could not register service")
                return service
            return self.setActiveService(service)
        
        for base in service.__bases__:
            if base in self.contracts:
                self.active_services[base] = service
        else:
            self.logger.info("error finding contract")
            service = None
        return service
    
    def _get_contract_by_service(self, service):
        base_set = set(service.__bases__)
        contract_set = set(self.contracts.keys())
        found = base_set.intersection(contract_set)
        return found

    def factory(self, service, *args, **kwds):
        """
        Instantiates the concrete service objects
        """
        ## get the contract related to this service
        contracts = self._get_contract_by_service(service)
        if not contracts:
            return ReturnCode("NotInMap")
        if len(contracts) > 1:
            return ReturnCode("UnexpectedResult", throw=True)
        contract = contracts.pop()
        
        if service == None:
            service = self.active_services[contract]
        else:
            if service not in self.services:
                return ReturnCode("NotFound")
        
        if not service:
            raise TaskFailure(ReturnCode("NoActiveService"))   
        return service(*args, **kwds)


############################################
## When this module gets imported, no matter
## where it gets imported, since it is a singleton
## we will always get this object, even if 
## a user inadvertently does something like:
## sm = importer.ServiceManager()
##
## If a user does do that, it will simply 
## return the manager here
############################################
manager = ServiceManager()


