
from interfaces.interfaces import FooIFace
from service.services import Foo
import manager.manager

mgr = manager.manager.manager

contracts = mgr.registerContract(FooIFace)
iface = mgr.registerService(Foo, FooIFace)
foo = mgr.factory(Foo, 10)
print foo
val = foo.doSomething(20)
print val

mgr.unregisterService(Foo, FooIFace)
 
foo2 = Foo(10)
print foo2