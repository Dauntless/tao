"""
The interfaces package holds the keys to defining the service contract that
a service component must implement.  In general, this package is used to
define an interface.  When an author wishes to create a new interface type,
it should be placed in this package.

In general, a new interface will import do something like:

usage::

    from abc import ABCMeta, abstractmethod
    from managed_iface import ManagedInterface
    
    class ITemplate(ManagedInterface):
        __metaclass__ = ABCMeta
        
        @abstractmethod
        def foo(self):
            pass
            
See also: service.managed_service
"""

from . import interfaces
from . import itask
from . import managed_iface
from . import status