"""
As simple as this module is, it defines the backbone of declaring all subclassed interfaces.

The idea here is that every class in the interfaces.interfaces module will inherit from this
abstract ManagedInterface.  Because ManagedInterface is an abstract class, it will not be
possible to accidentally instantiate one much less call a non-implemented method.  Moreover,
being an actual type, one can enforce that abstractmethods must be overridden in any subclass
"""

from abc import ABCMeta

class ManagedInterface(object):
    """
    All interfaces should be defined through this class
    """
    __metaclass__ = ABCMeta
    
    def __call__(self, *args, **kwargs):
        print("Can not instantiate directly")
        raise NotImplementedError()
