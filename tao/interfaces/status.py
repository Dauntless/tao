"""
Defines an interface to retrieve information about Fusion-io controllers
"""

from tao.interfaces.managed_iface import ManagedInterface


class StatusIFace(ManagedInterface):
    """
    classdocs
    """
    pass