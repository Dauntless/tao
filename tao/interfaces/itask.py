"""
@author: root
"""

from abc import ABCMeta, abstractmethod
from tao.interfaces.managed_iface import ManagedInterface


class ITask(ManagedInterface):
    """
    The ITask interface is used as a bridge between the old tau.task class and
    a new style task class.  By implementing this interface, a class should be
    able to act as a drop-in replacement for the older tau.task classes.
    
    In particular, we wish to do away with the complexity of the task and task
    proxy, add remote procedure calls, and also allow for the implementation
    of something other than threading (for example, multiprocessing)
    
    """
    __metaclass__ = ABCMeta
    
    @abstractmethod
    def get_time_remaining(self):
        """Returns the number of seconds left before this task times out.

        Returns None if no timeout has been specified.

        """
        pass
    
    @abstractmethod
    def get_task_path(self):
        """Return path for this task and create it if it does not exist.

        The task path is relative to the _task_root. An integer "generation"
        number is appended to the _task_root and incremented for each
        invocation (instance) of the task.

        This method has the side-effect of creating the task path if it
        does not already exist; thus the returned task path is guaranteed to
        exist.

        """
        pass
    
    @abstractmethod
    def get_timeout(self):
        """Return task-recommended timeout, in seconds.

        It is task-dependent what constitutes a recommended timeout. Generally,
        if a timeout is passed to the task constructor, it will be returned
        here; otherwise the task may compute a timeout based on other task
        parameters.

        This method may be called prior to starting the task thread.

        Note: task writers probably should not be reimplementing this method.
        See _compute_timeout() instead.

        """
        pass
    
    @abstractmethod
    def _compute_timeout(self):
        """Return a computed timeout for the task.

        Task subclasses should reimplement this method if they want to
        compute a timeout.  For example, a good candidate to reimpliment
        this method would be a task whose timeout would depend on the
        size of the device.

        Generally, to be safe, the timeout that is computed should be several
        (at least 3) times greater than the approximate worst case running
        time of the task.

        If tasks do not specify any timeout and do not reimpliment this
        method, the get_timeout method will call this version of the
        _compute_timeout method, which returns None.  Thus, the default is to
        wait forever, which occurs when a task that does not specify its own
        timeout.
        """
        pass
    
    @abstractmethod
    def start_time(self):
        """The time the task started running."""
        pass
    
    @abstractmethod
    def result(self):
        pass
    
    @abstractmethod
    def set_finished(self):
        """Provide exception handling for _set_finished."""
        pass
    
    @abstractmethod
    def _set_finished(self):
        """Notify a Task to finish.

        This implmementation does nothing. Task subclasses may reimplement
        this method if they implement an ongoing process and need to be
        notified when it is time to stop processing.

        This method should be called prior to joining with a Task thread.

        """
        pass
    
    @abstractmethod
    def set_timedout(self):
        pass
    
    @abstractmethod
    def run(self):
        """Executes subclass's _run() method and maps exceptions to appropriate
           task results."""
        pass
    
    @abstractmethod
    def _run(self):
        pass
    
    @classmethod
    @abstractmethod
    def call(cls, entity, *args, **kwargs):
        pass
    
    @classmethod
    @abstractmethod
    def call_many(cls, entities, *args, **kwargs):
        pass
