"""
@author: stoner
"""

from tao.interfaces.managed_iface import ManagedInterface
from abc import abstractmethod


class FooIFace(ManagedInterface):

    @abstractmethod
    def doSomething(self, x):
        pass


class Configurator(ManagedInterface):
    """
    Contract interface that exposes the common API for getting arguments
    """

    @abstractmethod
    def getConfig(self, config):
        """
        Gets the medium in which the arguments are held.  
        
        For example, if commandline, config might hold a dictionary of args, or
        read from the command line through argparse.  For YAML, config could
        be a path to a YAML config file
        """
        pass
    
    @abstractmethod
    def displayConfig(self):
        """
        Displays the output of self.config
        
        Mostly useful for debugging, this will pretty print in a human readable
        way what the source of the documentation is
        """
        pass
    
    @abstractmethod
    def getArgumentValue(self, key):
        """
        Given a key property, look up the value associated with the key
        
        Note that this does not imply a dictionary like key-value mapping.
        The idea is that all configurations have a notion of an element
        that contains data.  They key is the element, and the data in 
        the element is the value.  So this could also apply to hierarchical
        data like XML or YAML.
        """
        pass
    
    @abstractmethod
    def parseArgs(self):
        """
        Parses through the arguments held in self.config
        
        Depending on the medium, different parsers will be used.  For example,
        argparse for commandline, OptParser for ini style, or pyyaml for
        YAML files.
        """
        pass
    
    @abstractmethod
    def setArgument(self, name, **kwds):
        pass