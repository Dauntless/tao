'''
Created on Sep 9, 2013

@author: stoner
'''
import unittest

from manager.manager import ServiceManager
from interfaces.interfaces import FooIFace
from service.services import Foo
        
class TestManager(unittest.TestCase):
    """
    """

    def setUp(self):
        self.mgr = ServiceManager()
        self.mgr.registerContract(FooIFace)
        
    def tearDown(self):
        #self.mgr.unregisterService(service, contract)
        pass

    def testRegisterContract(self):
        self.assertEqual(self.mgr.contracts[FooIFace], [])
        self.assertTrue(FooIFace in self.mgr.contracts, "contract not in self.mgr.contracts")
        
    def testRegisterService(self):
        iface = self.mgr.registerService(Foo, FooIFace)
        #self.assertTrue(iface is self.mgr.contracts)
        self.assertTrue(Foo in self.mgr.services)
        
    def testFactory(self):
        iface = self.mgr.registerService(Foo, FooIFace)
        foo = self.mgr.factory(iface, Foo, 10)
        self.assertTrue(foo != None)
        ret = foo.doSomething(20)
        self.assertEquals(ret, 40)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()