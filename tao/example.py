from subprocess import Popen, STDOUT, PIPE
import sys
from subprocess import Popen, PIPE, STDOUT
import threading


def make_subprocess(cmd, shell=False):
    proc = Popen(cmd, shell=shell, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
    return proc


def proc_reader(proc):
    """Simple thread reader for stdout.  Since readline() will block until 
    the buffer is full and it gets flushed, we call this in a thread"""
    while proc.poll() is None:
        sys.stdout.write(proc.stdout.readline())
    # get the remaining output
    for line in proc.stdout.readlines():
        sys.stdout.write(line)

if __name__ == "__main__":
    p = make_subprocess(["iostat", "2", "4"])
    t = threading.Thread(target=proc_reader, args=(p,))
    t.daemon = False
    t.start()
