"""
Getting tired of recursively removing binary artifacts or log files?
"""
import os
import shutil
import sys

def remove_fileext(fileext, direc=os.getcwd()):
    """
    dir should be an absolute (not relative) path

    Args:
        fileext(string)- checks if entry ends with this extension
        direc(string)- absolute directory path

    Usage::

        from recursive_delete import remove_fileext
        remove_fileext(".pyc", direc="/opt/myrepo")

    Return:
        The number of files deleted
    """
    removed = 0
    for entry in os.listdir(direc):
        fullpath = os.path.sep.join([direc, entry])
        #print "checking path = {0}, entry = {1}".format(direc, entry)
        if entry.endswith(fileext) and os.path.isfile(fullpath):
            print "removing {0}".format(fullpath)
            removed += 1
            os.unlink(fullpath)
        if os.path.isdir(fullpath):
            #print "entering {0}".format(fullpath)
            remove_fileext(fileext, direc=fullpath)

    return removed

def remove_dir(dirname, direc=os.getcwd()):
    for entry in os.listdir(direc):
        fullpath = os.path.abspath(entry)
        if os.path.isdir(dirname) and dirname == entry:
            shutil.rmtree(fullpath)
        if os.path.isdir(fullpath):
            remove_dir(dirname, direc=fullpath)


if __name__ == "__main__":
    if not len(sys.argv) >= 2:
        print "usage: python recursive_delete.py '.zip'"
        print "usage: python recursive_delete.py '.pyc' /opt/some_dir"
        sys.exit(1)
    extension = sys.argv[1]
    directory = os.getcwd()
    if len(sys.argv) > 2:
        directory = sys.argv[2]
    print directory, len(sys.argv)
    remove_fileext(extension, direc=directory)
        
            
            
