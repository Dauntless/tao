"""
Utility script to retrieve the options from all the main.c(pp) fio utilities
"""

import re
import os

usage_patt = re.compile(r"usage\(")

## this rather gnarly regex will gather 4 groups
## 0: the short param (eg, -a)
## 1: a value the short param takes (optional, if not found, will be None)
## 2: the long param (eg. --all)
## 3: a value the long param takes (optional, None if not needed)
##
## Test strings
## test1 = '   -f<spec>    --format <spec>         Format output:\n'
## test2 = '   -d          --data-volume           Report the volume of data read and written.\n'
## test3 = "   -f, --file <file name>   Output file\n"
##
## Results:
## test1 = ('-f', '<spec>', '--format', '<spec>')
## test2 = ('-d', None, '--data-vol
params_patt = re.compile(r"\s+(\-\w)\s*,?\s*(<\w+>)?\s+(\-{2}\w+|\-{2}\w+\-\w+)?\s*(<[a-zA-Z0-9 ]+>)?\s+(.*)\\")

## For some unknown reason fio-matter's main.c doesn't follow the pattern of using <> to indicate 
## a value for a param.  If searching through fio-matter's main.c, then use this pattern instead
matter_patt = re.compile(r"\s+(\-\w)\s*,?\s*(<\w+>)?\s+(\-{2}\w+|\-{2}\w+\-\w+)?\s*([A-Z]+)?\s+(.*)\\")

class ArgType(object):
    def __init__(self, arg_groups=None):
        if arg_groups is None:
            self.short = ""
            self.long = ""
            self.description = ""
            self.parameterized = False
            self.groups = None
        else:
            self.short = arg_groups[0]
            if arg_groups[2] is not None:
                self.long = arg_groups[2]
            if arg_groups[1] or arg_groups[3]:
                self.parameterized = True
            self.description = arg_groups[4]
            self.groups = arg_groups
            
            
    def __iter__(self):
        for attr in dir(self):
            if not attr.startswith("__"):
                yield attr, getattr(self, attr)
            

def countBraces(line, braces):
    if "{" in line:
        braces.append(1)
    if "}" in line:
        braces.pop()
    return braces


def findUsage(line, patt=usage_patt):
    m = patt.search(line)
    if m:
        print "Got Usage"
    return 1 if m else 0      
        

def search_file(filename, patt=params_patt):
    print "Searching {0}".format(filename)
    got_usage = 0
    braces = []
    options = []
    with open(filename, "r") as fobj:
        for line in fobj.readlines():
            got_usage += findUsage(line, patt=usage_patt)
            braces = countBraces(line, braces)
            if got_usage == 0: 
                continue
            
            if got_usage == 1:
                p = patt.search(line)
                if p:
                    print p.groups()
                    argtype = ArgType(p.groups())
                    options.append(argtype)
             
            if len(braces) == 0 and len(options) > 0:
                break   
                  
    return options


def findMain(dirpath, mains=[]):
    if not os.path.isdir(dirpath):
        print "Must provide a directory path"
    for entry in os.listdir(dirpath):
        fullpath = os.path.sep.join([dirpath, entry])
        if os.path.isfile(fullpath):
            if entry == "main.c" or entry == "main.cpp":
                mains.append(fullpath)
        else:
            findMain(fullpath, mains)
                
                
if __name__ == "__main__":
    main_files = []
    findMain("/opt/fh-bodie/src/util", mains=main_files)
    for main_f in main_files:
        if "fio-matter" in main_f:
            opts = search_file(main_f, patt=matter_patt)
        else:
            opts = search_file(main_f, patt=params_patt)
        for i,opt in enumerate(opts):
            print i, ":", opt.description
            for k,v in opt:
                if k == "description":
                    continue
                print "\t", k, "=", v
                
        print ""
                
                
