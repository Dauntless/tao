import unittest
import sys,os

try:
    sys.path.append("..")
    import downloader as dl
except ImportError as ie:
    print "Could not import downloader"
    sys.exit(0)

TEST_URL = "https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py"
TEST_FILE = "ez_setup.py"

class TestDownloader(unittest.TestCase):

    def tearDown(self):
        if os.path.exists(TEST_FILE):
            print "{0} exists, removing".format(TEST_FILE)
            os.remove(TEST_FILE)


    def test_downloadURL(self):
        dl.Downloader.downloadURL(TEST_URL)
        self.assertTrue(os.path.exists(TEST_FILE))


if __name__ == "__main__":
    unittest.main()
