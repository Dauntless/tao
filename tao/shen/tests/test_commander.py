import unittest, sys, time
try:
    sys.path.append("..")
    import commander as cdr
except ImportError as ie:
    print "Could not load commander module"
    sys.exit(0)

class TestCommand(unittest.TestCase):
    def test_shortCmd(self):
        cmd = cdr.Command("ls -al")
        p = cmd()
        self.assertEquals(p[0].returncode, 0)

    def test_inlineCmd(self):
        cmd = cdr.Command()
        p = cmd("which python")
        self.assertEquals(p[0].returncode, 0)

    def test_longCmd(self):
        cmd = cdr.Command("python dummy.py")
        res = cmd(block=False)
        print_thread = cmd.printRunningProc(res)
        print_thread.start()
        print_thread.join()
            
        self.assertEquals(res[0].returncode, 0)
        

    
if __name__ == "__main__":
    unittest.main()
