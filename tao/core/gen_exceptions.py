"""
Some specialized exceptions

Author:  Sean Toner
Date: 08/0/13
"""

class Override(Exception):
    def __init__(self, msg=""):
        if not msg:
            self._msg = "Can not run a non-overridden method in a subclass"
        else:
            self._msg = msg
        super(Override, self).__init__(self._msg)


class UnhandledException(Exception):
    def __init__(self, msg=""):
        if not msg:
            self._msg = "Unhandled exception thrown from calling function"
        else:
            self._msg = msg
        super(UnhandledException, self).__init__(self._msg)
            