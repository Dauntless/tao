"""
The codehelper module is meant to help with other source code tools such as cscope,
ctags, eclipse, pypi, etc.  If it's a software tool, then codehelper will try to install
it or generate artifact files for other tools (eg .cscope files or TAGS files)
"""

from . downloader import *
from . decorators import override
from tao.tasks.commander import Command


class CodeHelper(object):
    """
    This class presents a common API for an OS to be able to install 
    various software.
    """
    def __init__(self, cmd_class):
        self.cmd_class = cmd_class

    @override
    def getPackageInfo(self, cmd, name, ver=None):
        pass

    @override
    def findPackage(self, cmd, name, ver=None):
        """
        Searches for a package in the distros's repo.  Returns
        """
        pass

    @override
    def removePackage(self, cmd, name, ver=None):
        """
        Removes a package from the distros native package manager
        """
        pass

    @override
    def installPackage(self, cmd, name, ver=None):
        """
        Installs a package from distro's native repo
        """
        pass

    @override
    def installPythonDevel(self):
        """
        Installs the python development libs
        """
        pass

    @override
    def installEmacs(self):
        pass

    @override
    def installPythonFromSrc(self):
        pass

    @override
    def installPyPy(self):
        pass

    @override
    def installClang(self):
        pass

    @override
    def installBoost(self):
        pass

    @override
    def installJDK(self):
        pass

    def pipInstaller(self, name ):
        """
        """
        cdr = self.cmd_class()
        result = cdr("pip install -U --no-allow-insecure --allow-external {0}".format(name))
        
    def installSetupTools(self):
        """
        """
        setuptools = "https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py"
        Downloader.downloadURL(setuptools)
        cdr = self.cmd_class()
        result = cdr("python ez_setup.py")


class RHEL6Family(CodeHelper):
    """
    Subclass for any of the RHEL family distros (OEL, RHEL, Centos, Fedora)
    """
    def __init__(self, cmd_class):
        super(RHEL6Family, self).__init__(cmd_class)
        self.cdr = self.cmd_class()


    def installLib(self, libname):
        """
        Installs a library
        """
        install = self.pkginstall + libname
        res = self.cdr(install)

    @override
    def installPythonDevel(self):
        """
        Installs the python development libs
        """
        pass

    @override
    def installEmacs(self):
        pass

    @override
    def installPythonFromSrc(self):
        pass

    @override
    def installPyPy(self):
        pass

    @override
    def installClang(self):
        pass

    @override
    def installBoost(self):
        pass
   
    
if __name__ == "__main__":
    ch = CodeHelper(Command)
    rh = RHEL6Family(Command)
    rh.installSetupTools()
    res = rh.pipInstaller("virtualenv", block=False)
    print_thread = rh.printRunningProc(res)
    print_thread.join()  ## this is here, so we don't exit out the main thread first

