from urllib.request import urlopen, urlparse
import os
from tao.tasks import commander
from tao.core.logger import glob_logger
from tao.tasks.commander import Result


class Downloader(object):
    """
    This is a class to help with downloading and installing various artifacts.  These 
    artifacts may be from the web, an ftp server, or even scp'ed or on a 
    """
    
    ez_setup_path = "https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py"
    pip_path = "https://raw.github.com/pypa/pip/master/contrib/get-pip.py"

    def __init__(self, executor=None, osinfo=None):
        if not executor:
            self.executor = commander.Command()
        else:
            self.executor = executor
        self.logger = self.executor.logger
        self.osinfo = osinfo

   
    @staticmethod
    def isMercurialInstalled():
        executor = commander.Command()
        res = executor("hg --version", showerr=False, showout=False)
        return not res.proc.returncode


    def installMercurial(self):
        hg_src_path = "http://mercurial.selenic.com/release/mercurial-2.7.tar.gz"
        result = Downloader.downloadURL(hg_src_path, output_dir="downloads",
                                        binary=True)
        if not result:
            self.logger.error("Unable to download mercurial source")
        
        ## verify the python devel libs are there

    @staticmethod
    def downloadURL(urlpath, output_dir=".", binary=False):
        thing = None
        try:
            thing = urlopen(urlpath)
        except Exception as e:
            print(str(e))
            return
            
        parsed = urlparse.urlparse(urlpath)
        filename = os.path.basename(parsed.path)
        writemod = "wb" if binary else "w"

        fobj = thing.read()
        if output_dir != ".":
            if not os.path.exists(output_dir):
                glob_logger.error("{0} does not exist".format(output_dir))
                glob_logger.error("Writing file to {0}".format(os.getcwd()))
            else:
                filename = "/".join([output_dir,filename])
        with open(filename, writemod) as downloaded:
            downloaded.write(fobj)

        return os.path.exists(filename)

    @staticmethod
    def isSetuptoolsInstalled():
        executor = commander.Command("easy_install --help")
        res = executor(showout=False, showerr=False)
        return not res.proc.returncode

    def isPipInstalled(self):
        res = self.executor("pip --help")
        return not res.proc.returncode

    def installSetuptools(self, ezsetup="ez_setup.py"):
        if not self.downloadURL(Downloader.ez_setup_path):
            err = "Could not download ez_setup.py"
            self.logger.error(err)
            return Result(-1, err)
        
        cmd = "python {0}".format(ezsetup)
        return self.executor(cmd, checkresult=(True,0))

    def installPip(self, pipfile="get-pip.py"):
        if not self.downloadURL(Downloader.pip_path):
            err = "Could not download {0}".format(pipfile)
            self.logger.error(err)
            return Result(-1, err)
        
        cmd = "python {0}".format(pipfile)
        self.executor(cmd, checkresult=(True,0))

    def installPythonLib(self, scriptname):
        """
        This will install distutils based python scripts
        that use setup.py
        """
        ## FIXME:  we may want to use a different python
        ## for example python3, or a virtualenv python
        cmd = "{0} {1} install".format(self.python, scriptname)
        res = self.executor(cmd, checkresult=(True,0))

    def checkForPythonDevel(self):
        try:
            from distutils import sysconfig as ds
            vars = ds.get_config_vars()
            if vars.has_key("INCLUDEPY"):
                if "Python.h" in os.listdir(vars["INCLUDEPY"]):
                    return True
            return False
        except ImportError:
            le = self.logger.error
            le("Can not determine if python devel libs installed")

    def pipCmd(self, pkgnames, cmd="install", pip_args=[]):
        try:
            from setuptools import find_packages
            import pip
        except ImportError as ie:
            glob_logger.error(ie.msg)

        pip_args.append(cmd)
        if isinstance(pkgnames, str):
            pip_args.append(pkgnames)
        else:
            ## concatenate the lists
            pip_args += [pkg for pkg in pkgnames]
 
        msg = "Running pip " + " ".join(pip_args)
        glob_logger.info(msg)
        pip.main(initial_args=pip_args)


    def _hgCmd(self, urlpath, cmd="clone", opts="", reponame=""):
        """
        Simple wrapper around mercurial
        """
        cmd = "hg {0} {1} {2} {3}".format(cmd, opts, urlpath, reponame)
        res = self.executor(cmd, checkresult=(True,0)) 
        return res

    def hgClone(self, urlpath):
        return self._hgCmd(urlpath)


    def hgPull(self, repo_dir, update=True):
        hgcmd = "pull"
        pull_opts = "-u" if update else ""
        owd = os.getcwd()
        os.chdir(repo_dir)
        result = self._hgCmd("", cmd=hgcmd, opts=pull_opts)
        os.chdir(owd)
        return result

    def getRecipe(self, pkgname):
        """
        This function will retrieve a dependencies file
        """
        pass 
