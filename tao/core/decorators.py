"""
A collection of various decorators.  These should probably be put into separate modules

Author: Sean Toner
Date: 08/18/2013
"""


def assertArgs( pos_args, key_reqs ):
    """
    Takes a map of keyname, value types.  Compares the wrapped
    function's arguments to verify that any kwds match.  Note that the logic is a 
    little tricky, because when a user calls a function, he does not need to specify
    the keyword name.  For example, imagine this definition::
    
        def isAFactor(number, factor=2):
            return number % factor
            
    A user may call this function like::
    
        isAFactor(100, 5)        ## example 1
        isAFactor(100,factor=5)  ## example 2
        
    Because of this, the pos_args must also contain all the type information for the
    keyword name arguments in the order they are listed.  When a user calls the
    function as in the first example, the positional args are stored in the same
    order they were specified as keyword argument names.  

    *args*
        fn(function)- An implicit param, it is the function to be decorated
        pos_args(list) - a list containing the types of positional and keyword names (in order)
        key_reqs(dict)- a dict, where key is keyword name, value is required type

    *return*
        returns whatever the decorated function does, or KeyError if fails

    Usage::
    
        @assertKwds([int,int], { "factor" : int })
        def isAFactor(number, factor=2):
            return not number % factor
                
        ## All of these will raise an error
        fctr = "john"
        divides_evenly = isAFactor(100,fctr)
        divides_evenly = isAFactor("john", 2)
        divides_evenly = isAFactor(10, factor="5")
        
        ## remember the implicit "self" in methods
        class Foo(object):
          @assertArgs([Foo, str, str], {})  ## notice Foo as the first arg
          def nameMatch(self, first, second):
            return first == second
            
        foo = Foo()
        foo.nameMatch("sean", 100)  ## will fail
        
    PreCondition-
      Because of the way methods implicitly pass in "self" as the first arg, this decorator 
    will not work on methods in a class unless you do something funky like this::
    
      class Foo(object):
        pass
        
      @assertArgs([Foo, str, str], {})
      def nameMatch(self, first, second):
        return first == second
        
      Foo.nameMatch = nameMatch
         
    """
    def wrap(fn):
        def wrapper(*args, **kwds):
            failed = False            
            msg = "Invalid type {0} for {1} {2}. Should be {3}"
            print(args)
            print(key_reqs)
            print(kwds)
            try:
                for k,v in key_reqs.items():
                    if not kwds:  
                        continue
                    elif not kwds.has_key(k):
                        continue
                    if not isinstance(kwds[k], v):
                        print(msg.format(type(kwds[k]), "keyword", k, str(v)))
                        failed = True
            except KeyError as ke:
                err = "Faulty assertion.  keyword arg {0} does not exist"
                print(err.format(ke.args[0]))
                failed = True
            
            try:       
                for i,t in enumerate(pos_args):
                    if not isinstance(args[i], pos_args[i]):
                        print(msg.format(type(args[i]), "position", i, str(pos_args[i])))
                        failed = True
            except IndexError as ie:
                pass
            
            if failed:
                raise TypeError
            
            ################################
            ## now that we have verified the args, call the decorated fn
            ################################
            return fn(*args, **kwds)
        return wrapper
    return wrap


def except_catch(exc_type, handler=None):
    """The idea behind this function is to do the exception outside of the function
    
    When you want to guarantee that when a function is called, this can be used. Note
    that it is only useful when handler is not None.  This allows the logic of the 
    handling the exception to exist somewhere else
    
    *args*
        exc_type(Exception)- The exception type to handle (eg, KeyError)
        handler(function)- the exception handler. it takes the same args as the 
          called function
          
    *return*
        Depends on the handler.  If no handler, just 
    """
    def wrap(fn):
        def wrapper(*args, **kwds):
            try:
                return fn(*args, **kwds)
            except exc_type as ex:
                if handler:
                    return handler(*args, **kwds)
                else:
                    print("No")
                    raise
        return wrapper
    return wrap


def override(fn):
    """
    Use this to specify if a method in a bsae class must be overridden
    """
    def wrapper(*args, **kwargs):
        raise NotImplementedError()
    return wrapper


def fixme(fn):
    """
    Apply to functions which are inherently broken
    """
    def wrapper(*args, **kwargs):
        print("This function is broken!! Please fix")
        print("calling {0}".format(fn))
    return wrapper


def debug_args(fn):
    """Decorator to display all arguments"""
    def wrapper(*args, **kwargs):
        for i,x in enumerate(args):
            print(i,":","x")
        for k,v in kwargs.items():
            print(k,"=",v)
        return fn(*args, **kwargs)
    return wrapper


def make_iterable_class(cls):
    """
    Instead of monkey patching a class and just inserting cls.__iter__ = __iter__
    this creates a subclass and makes it iterable.  It's now a new type and can be
    used wherever the original class was
    """
    class Clazz(cls):
        def __init__(self, *args, **kwds):
            super(self, Clazz).__init__(*args, **kwds)

        def __iter__(self):
            for x in dir(self):
                if not x.startswith("_"):
                    yield x, getattr(self, x)

    return Clazz


class IterableMixin(object):
    """
    And here's another way to do it
    """
    def __iter__(self):
        for x in dir(self):
            if not x.startswith("_"):
                yield x, getattr(self, x)


if __name__ == "__main__":
    @assertArgs([int, int], {"factor": int})
    def isAFactor(number, factor=2):
        return number % factor
    
    try:
        isAFactor(100, "j")
    except:
        pass
