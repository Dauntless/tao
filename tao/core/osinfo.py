"""
This is a class to help get platform information about the system
"""
import platform, os, sys
import re
from decorators import override

class OSInfoFactory(object):
    """
    """
    @staticmethod
    def getOSName():
        return platform.system().lower()


    @staticmethod
    def makeOSInfo(name):
        osname = OSInfoFactory.getOSName()
        if osname == "linux":
            distro = platform.linux_distribution().lower()
            if distro == "centos":
                return CentOSInfo()
            
    

class OSInfo(object):
    """
    An abstract base class which is subclassed based on the OS specific
    type, and for linux the specific distribution type
    """
    def __init__(self, name, cmd_class):
        self.ostype = name
        ## The class from which to construct executors
        ## This follows the IoC pattern.  Don't instantiate
        ## the concrete class, let the caller decide
        self.cmd_class = cmd_class  

        self.distro = ""
        self.major = ""
        self.minor = ""
        self.python_exe = ""
        self.python_ver = ""
        self.gcc_ver = ""
        self.kernel = ""
        self.jython = ""  ## jython?
        self.pypy = ""    ## pypy
        self.ironpy = ""  ## ironpython
        
        self.java_home = ""  ## JDK directory
        
        self.packman = ""  ## the package manager
        self.pkginstall = ""
        self.pkgsearch = ""
        self.pkginfo = ""
        self.pkgremove = ""
        self.codehelper = None

        self.getVersionInfo()

    def __iter__(self):
        for x in dir(self):
            if not x.startswith("_"):
                yield x, getattr(self, x)


    def __repr__(self):
        display = ""
        for x in dir(self):
            if not x.startswith("_"):
                display += x + " = " + str(getattr(self, x)) + "\n"
        return display
            
            


    def getPythonVersion(self):
        self.python_ver = platform.python_version_tuple()
        return self.python_ver


    def getVersionInfo(self):
        version = platform.linux_distribution()[1]
        version_patt = re.compile(r"(\d+)\.(\d+)")
        m = version_patt.search(version)
        if not m:
            self.major = version
        else:
            self.major,self.minor = m.groups()

        return self.major,self.minor
    


class CentOSInfo(OSInfo):
    def __init__(self):
        super(CentOSInfo, self).__init__("linux")
        self.distro = "centos"
        self.packman = "yum"
        self.pkginstall = " ".join([self.packman, "install", "-y "])
        self.pkgsearch = " ".join([self.packman, "search "])
        self.pkginfo = " ".join([self.packman, "info "])
        self.pkgremove = " ".join([self.packman, "erase", "-y"])

        #self.codehelper = RHEL6Family(self.commander)



class OpenSuseInfo(OSInfo):
    def __init__(self):
        super(OpenSuseInfo, self).__init__("linux")
        self.distro = "opensuse"
        self.packman = "zypper"
        
    
