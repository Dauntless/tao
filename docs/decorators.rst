.. automodule:: decorators
   :members:


The decorators module is a catch-all for any useful kinds of decorators.  It should probably
be broken up, or put into other modules in a more logical fashion

