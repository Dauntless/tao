.. automodule:: logger
   :members:

The logger module is a utility wrapper around python's logging module to help make simple
loggers.  It only implements a basic stream and file handler.
