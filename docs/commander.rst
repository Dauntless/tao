.. automodule:: commander
   :members:

The commander module primarily provides support via the Command class.  This class
is meant to be used to make launching subprocesses more easily, including the ability
to launch long running processes, not block, but still capture output (if the child
process does not buffer its output).
