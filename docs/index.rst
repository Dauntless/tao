.. Shen documentation master file, created by
   sphinx-quickstart on Wed Aug 21 10:03:40 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Shen's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

   commander
   decorators
   codehelper
   logger



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

