__author__ = 'stoner'

import time

counter = 5

while counter > 0:
    print(counter)
    time.sleep(1)
    counter -= 1

    if counter <= 0:
        while True:
            ans = raw_input("Reset counter? 0 to quit: ")
            try:
                counter = int(ans)
                break
            except ValueError:
                print("value must be an integer")