import sys
from subprocess import Popen, PIPE, STDOUT
import threading


def make_subprocess(cmd, shell=False):
    proc = Popen(cmd, shell=shell, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
    return proc


def proc_reader(proc):
    while proc.poll() is None:
        sys.stdout.write(proc.stdout.read(1))
    # get the remaining output
    for line in proc.stdout.readlines():
        sys.stdout.write(line)


if __name__ == "__main__":
    p = make_subprocess(["python", "dummy.py"])
    t = threading.Thread(target=proc_reader, args=(p,))
    t.daemon = False
    t.start()

    # The not recommended way to do this:
    #import time
    #while p.poll() is None:
    #    time.sleep(1)

    # Don't join(), otherwise the script will terminate because the parent thread is done at this point
    # By setting daemon = False, we will wait for the child thread to finish